import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { KYCDetailsComponent } from '../kyc-details/kyc-details.component';

declare var $: any;

@Component({
  selector: 'app-verify-application',
  templateUrl: './verify-application.component.html',
  styleUrls: ['./verify-application.component.css']
})
export class VerifyApplicationComponent implements OnInit {
  @ViewChild(KYCDetailsComponent) kycAccepted:any;
  centralKYCForm: FormGroup;
  aadharForm: FormGroup;
  receivedAddress: FormGroup;
  nextForm: FormGroup;
  
  formSubmitted:boolean =false;
  getKYC:boolean = true;
  checked: boolean = true;
  enterAddress:boolean = false;
  nextAddress:boolean = true;
  detailsReceived:boolean = false;
  hidepanelClicked:boolean = false;
  flag:boolean = true;

  kycNumber:any;
  kyc:any;
  key:any;

  receivedAddressName:any = 'current address';
  notReceivedAddressName:any = 'permanent address'
  
  constructor(private fb: FormBuilder, public router: Router) {
    
    this.centralKYCForm = this.fb.group({
      CKYCNumber: [this.kycNumber, Validators.required]
    });

    this.aadharForm = this.fb.group({
      aadharNumber: [this.kycNumber, Validators.required],
      aadharDeclaration: [this.checked, Validators.required]
    });

    this.receivedAddress = this.fb.group({
      address1: ['', Validators.required]
    });

    this.nextForm = this.fb.group({
      addressN1: ['', Validators.required],
      pincodeN: ['', Validators.required],
      cityN: ['', Validators.required],
      stateN: ['', Validators.required]
    });

   }

  ngOnInit(): void {
    console.log(localStorage.getItem(this.key));
    if(localStorage.getItem(this.key)=='viwed'){
      this.detailsReceived = true;
    } else {
      localStorage.setItem(this.kyc, '');
      this.detailsReceived = false;
    }
    if(this.getKYC){
      this.kycNumber= "KC88557656"
    }
    this.saveform();
  }

  public saveform() {
    this.formSubmitted = true; 
		if (this.centralKYCForm.valid) {      
      localStorage.setItem(this.kyc, 'details');
      this.router.navigate(['KYC-details']);
		}
	}

  generateOTP() {
    this.formSubmitted = true; 
		if (this.aadharForm.valid) {
      console.log("Generate OTP");
		}
  }
  
  openConditions(event:any, getValue:any) {
    getValue = event.target.checked;
    this.enterAddress = !this.enterAddress;    
  }

  hideAccordion() {
    this.hidepanelClicked= !this.hidepanelClicked; 

  }
  
}


