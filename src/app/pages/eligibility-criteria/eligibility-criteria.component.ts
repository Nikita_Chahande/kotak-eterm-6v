import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

import { SorryModalComponent } from 'src/app/components/sorry-modal/sorry-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { ToWords } from 'to-words';

declare var $: any;
// const toWords = new ToWords();

@Component({
  selector: 'app-eligibility-criteria',
  templateUrl: './eligibility-criteria.component.html',
  styleUrls: ['./eligibility-criteria.component.css'],
})
export class EligibilityCriteriaComponent implements OnInit {
  @ViewChild('content')
  content!: TemplateRef<any>;
  eligibilityCriteriaForm: FormGroup;
  formSubmitted = false;
  customerAge = 0;
  minDate = { year: 1920, month: 1, day: 1 };
  acceptPolicy = true;
  annual: any = '5,00,000';
  annualIncome: any = '';
  outputWords = '';
  private modalRef: any;
  closeResult = '';

  $: any;

  reasonsArray = [
    {
      title: 'Title 1',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore',
      icon: 'assets/images/place-holder.png',
    },
    {
      title: 'Title 2',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore',
      icon: 'assets/images/place-holder.png',
    },
    {
      title: 'Title 3',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore',
      icon: 'assets/images/place-holder.png',
    },
  ];
  occupationStatuses = [
    { label: 'Professional', value: 'Professional' },
    { label: 'Self Employed', value: 'Self Employed' },
    { label: 'Student', value: 'Student' },
    { label: 'Housewife', value: 'Housewife' },
    { label: 'Retired', value: 'Retired' },
    { label: 'Salaried', value: 'Salaried' },
  ];

  incomeStatuses = [
    { label: '5,00,000', value: '500000' },
    { label: '6,00,000', value: '600000' },
    { label: '7,00,000', value: '700000' },
    { label: '8,00,000', value: '800000' },
    { label: '9,00,000', value: '900000' },
  ];

  educationStatuses = [
    { label: 'Graduate', value: 'graduate' },
    { label: 'Undergraduate', value: 'undergraduate' },
    { label: 'Postgraduate', value: 'postgraduate' },
  ];

  nationalityStatuses = [
    { label: 'Resident indian', value: 'indian' },
    { label: 'Non-resident indian', value: 'nonIndian' },
  ];

  unitOne = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
  unitTwo = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];



  constructor(
    public router: Router,
    private fb: FormBuilder,
    public modalService: NgbModal
  ) {
    this.eligibilityCriteriaForm = this.fb.group({
      occupation: ['', Validators.required],
      income: [this.annualIncome, Validators.required],
      education: ['', Validators.required],
      nationality: ['', Validators.required],
      pincode: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    $('html, body').animate(
      {
        scrollTop: 0,
      },
      500
    );
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});
    //console.log(this.inWords(1234));
  }

  // navigate() {
  //   this.router.navigate(['customize-plan']);
  // }

  assignDropdownVal(field: string | number, val: any) {
    this.eligibilityCriteriaForm.controls[field].setValue(val);
  }

  navigate() {
    this.formSubmitted = true;
    const checkEligibility = this.eligibilityCriteriaForm.controls[
      'income'
    ].value.replace(/,/g, '');
    if (this.eligibilityCriteriaForm.valid) {
      console.log(parseInt(checkEligibility));
      if (parseInt(checkEligibility) <= 30000) {
        this.modalRef = this.modalService.open(this.content, {
          centered: true,
        });
        this.modalRef.result.then(
          (result: any) => {
            this.closeResult = `Closed with: ${result}`;
          },
          (reason: any) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            console.log(this.closeResult);
          }
        );
      } else {
        this.router.navigate(['customize-plan']);
      }
    } else {
      setTimeout(() => {
        this.moveToError();
      }, 500);
    }
  }

  back() {
    this.router.navigate(['landing']);
  }

  validate(evt: any) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  moveToError() {
    // var elt = $(".errorInput");
    // if (elt.length) {
    //   $('html, body').animate({
    //     scrollTop: (elt.first().offset().top) - 90
    //   }, 500);
    // }
  }

  isNumberKey(event: any) {
    var charCode = event.which ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  numberWithCommas() {
    if (this.eligibilityCriteriaForm.controls['income'].value != '') {
      var result = this.eligibilityCriteriaForm.controls[
        'income'
      ].value.replace(/,/g, '');
      result = result.toString();
      var lastThree = result.substring(result.length - 3);
      var otherNumbers = result.substring(0, result.length - 3);
      if (otherNumbers != '') lastThree = ',' + lastThree;
      var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
      this.eligibilityCriteriaForm.controls['income'].setValue(res);
      
      // this.outputWords = toWords.convert(res.replace(/,/g, ''));
    } else {
      this.outputWords = '';
    }
  }

  inWords () {
    if ((this.eligibilityCriteriaForm.controls['income'].value).length > 0) {
      var num = this.eligibilityCriteriaForm.controls[
        'income'
      ].value.replace(/,/g, '');
      if ((num = num.toString()).length > 9) return 'overflow';
      let n:any;
      n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
      if (!n) return; var str = '';
      str += (n[1] != 0) ? (this.unitOne[Number(n[1])] || this.unitTwo[n[1][0]] + ' ' + this.unitOne[n[1][1]]) + 'crore ' : '';
      str += (n[2] != 0) ? (this.unitOne[Number(n[2])] || this.unitTwo[n[2][0]] + ' ' + this.unitOne[n[2][1]]) + 'lakh ' : '';
      str += (n[3] != 0) ? (this.unitOne[Number(n[3])] || this.unitTwo[n[3][0]] + ' ' + this.unitOne[n[3][1]]) + 'thousand ' : '';
      str += (n[4] != 0) ? (this.unitOne[Number(n[4])] || this.unitTwo[n[4][0]] + ' ' + this.unitOne[n[4][1]]) + 'hundred ' : '';
      str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (this.unitOne[Number(n[5])] || this.unitTwo[n[5][0]] + ' ' + this.unitOne[n[5][1]]) : '';
      this.outputWords = str;
    }
}

  goHome() {
    this.router.navigate(['landing']);
  }

  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }
}
