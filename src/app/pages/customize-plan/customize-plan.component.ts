import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  FormsModule,
} from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HigherPremiumComponent } from 'src/app/components/higher-premium/higher-premium.component';
import { Tab } from 'src/app/components/tabset/tabset.component';
// import { ToWords } from 'to-words';

declare var $: any;
// const toWords = new ToWords();

@Component({
  selector: 'app-customize-plan',
  templateUrl: './customize-plan.component.html',
  styleUrls: ['./customize-plan.component.css'],
})
export class CustomizePlanComponent implements OnInit {
  tabs: Tab[] = [
    {
      title: 'HTML',
      active: true,
      iconClass: 'fab fa-html5',
      content: `<strong>HTML(HyperText Markup Language)</strong> is the most basic building block of the Web.
        It describes and defines the content of a webpage along with the basic layout of the webpage.
        Other technologies besides HTML are generally used to describe a web page's
        appearance/presentation(CSS) or functionality/ behavior(JavaScript).`
    },
    {
      title: 'CSS',
      active: false,
      iconClass: 'fab fa-css3',
      content: `<strong>Cascading Style Sheets(CSS)</strong> is a stylesheet language used to describe
        the presentation of a document written in HTML or XML (including XML dialects
        such as SVG, MathML or XHTML). CSS describes how elements should be rendered on screen,
        on paper, in speech, or on other media.`
    },
    {
      title: 'JavaScript',
      active: false,
      iconClass: 'fab fa-js-square',
      content: `<strong>JavaScript(JS)</strong> is a lightweight interpreted or JIT-compiled programming
        language with first-class functions. While it is most well-known as the scripting
        language for Web pages, many non-browser environments also use it, such as Node.js,
        Apache CouchDB and Adobe Acrobat. JavaScript is a prototype-based, multi-paradigm,
        dynamic language, supporting object-oriented, imperative, and declarative
        (e.g. functional programming) styles.`
    }
  ];

  
  @ViewChild('immediate')
  immediate!: TemplateRef<any>;

  @ViewChild('recurring')
  recurring!: TemplateRef<any>;

  @ViewChild('increasingRecurring')
  increasingRecurring!: TemplateRef<any>;

  customizePlanForm: FormGroup;
  formSubmitted = false;
  customerAge = 0;
  minDate = { year: 1920, month: 1, day: 1 };
  acceptPolicy = true;
  selectedFreq = 'Annually';
  private modalRef: any;
  closeResult = '';

  annualsum = '50,00,000';

  outputWords = '';

  sumStatuses = [
    { label: '5,00,000', value: '500000' },
    { label: '6,00,000', value: '600000' },
    { label: '7,00,000', value: '700000' },
    { label: '8,00,000', value: '800000' },
    { label: '9,00,000', value: '900000' },
  ];

  policyStatuses = [
    { label: '40 years', value: '40' },
    { label: '50 years', value: '50' },
    { label: '60 years', value: '60' },
    { label: '70 years', value: '70' },
  ];

  payingStatuses = [
    { label: 'Single Premium', value: 'SinglePremium', offer: 'Save 2%' },
    { label: '5 years', value: '5years', offer: 'Save 10%' },
    { label: '10 years', value: '10years', offer: 'Save 6%' },
    { label: '40 years', value: '40years', offer: 'Same as policy term' },
  ];

  feqStatuses = [
    { label: 'Annually', value: 'annually' },
    { label: 'Monthly', value: 'monthly' },
  ];

  payoutStatuses = [
    {
      label: 'Immediate',
      value: 'immediate',
      desc: 'Receive the entire coverage amount in one single payout.',
      id: 1,
    },
    {
      label: 'Level Recurring Payout',
      value: 'LevelRecurringPayout',
      desc: 'Receive a part of the coverage amount as a lump sum payout and the rest in monthly instalments.',
      id: 2,
    },
    {
      label: 'Increasing Recurring Payout',
      value: 'IncreasingRecurringPayout',
      desc: 'Receive a part of the coverage amount as a lump sum payout and the rest in increasing monthly instalments.',
      id: 3,
    },
  ];

  buttons = {
    pair1: 'BACK',
    pair2: 'PROCEED',
  };

  unitOne = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
  unitTwo = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

  values: any;

  constructor(
    public router: Router,
    private fb: FormBuilder,
    public modalService: NgbModal,
    
  ) {
    this.customizePlanForm = this.fb.group({
      sum: ['50,00,000', Validators.required],
      policy: ['40 years', Validators.required],
      paying: ['40 years', Validators.required],
      frequency: ['Anually', Validators.required],
      payout: ['Immediate', Validators.required],
    });
  }

  ngOnInit(): void {
    $('html, body').animate(
      {
        scrollTop: 0,
      },
      500
    );
    this.inWords();
    $('[data-toggle="tooltip"]').tooltip({container: 'body'})
  }

  assignDropdownVal(field: string | number, val: any) {
    this.customizePlanForm.controls[field].setValue(val);
  }

  // statusNavigate() {
  //   this.router.navigate(['add-ons']);
  // }

  statusNavigate() {
    this.formSubmitted = true;
    if (this.customizePlanForm.valid) {
      this.router.navigate(['add-ons']);
    } else {
      setTimeout(() => {
        this.moveToError();
      }, 500);
    }
  }

  back() {
    this.router.navigate(['eligibility-criteria']);
  }

  validate(evt: any) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  moveToError() {
    // var elt = $(".errorInput");
    // if (elt.length) {
    //   $('html, body').animate({
    //     scrollTop: (elt.first().offset().top) - 90
    //   }, 500);
    // }
  }

  checkFreq(field: string | number, val: any, content: any) {
    this.customizePlanForm.controls[field].setValue(val);
    if (val == 'Monthly') {
      this.modalRef = this.modalService.open(content, { centered: true });
      this.modalRef.result.then(
        (result: any) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason: any) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          console.log(this.closeResult);
        }
      );
    }
  }
  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }

  selectFreq(val: any) {
    this.selectedFreq = val;
    console.log(this.selectedFreq);
    this.modalRef.close();
  }

  close() {
    this.modalRef.close();
  }

  isNumberKey(event: any) {
    var charCode = event.which ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  numberWithCommas() {
    if (this.customizePlanForm.controls['sum'].value != '') {
      var result = this.customizePlanForm.controls['sum'].value.replace(
        /,/g,
        ''
      );
      result = result.toString();
      var lastThree = result.substring(result.length - 3);
      var otherNumbers = result.substring(0, result.length - 3);
      if (otherNumbers != '') lastThree = ',' + lastThree;
      var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
      this.customizePlanForm.controls['sum'].setValue(res);

      // this.outputWords = toWords.convert(res.replace(/,/g, ''));
    } else {
      this.outputWords = '';
    }
  }

  inWords () {
    if ((this.customizePlanForm.controls['sum'].value).length > 0) {
      var num = this.customizePlanForm.controls[
        'sum'
      ].value.replace(/,/g, '');
      if ((num = num.toString()).length > 9) return 'overflow';
      let n:any;
      n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
      if (!n) return; var str = '';
      str += (n[1] != 0) ? (this.unitOne[Number(n[1])] || this.unitTwo[n[1][0]] + ' ' + this.unitOne[n[1][1]]) + 'crore ' : '';
      str += (n[2] != 0) ? (this.unitOne[Number(n[2])] || this.unitTwo[n[2][0]] + ' ' + this.unitOne[n[2][1]]) + 'lakh ' : '';
      str += (n[3] != 0) ? (this.unitOne[Number(n[3])] || this.unitTwo[n[3][0]] + ' ' + this.unitOne[n[3][1]]) + 'thousand ' : '';
      str += (n[4] != 0) ? (this.unitOne[Number(n[4])] || this.unitTwo[n[4][0]] + ' ' + this.unitOne[n[4][1]]) + 'hundred ' : '';
      str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (this.unitOne[Number(n[5])] || this.unitTwo[n[5][0]] + ' ' + this.unitOne[n[5][1]]) : '';
      this.outputWords = str;
    }
  }

  relativeModal(id: any) {
    console.log(id);

    if (id == 1) {
      this.modalRef = this.modalService.open(this.immediate, {
        centered: true,
      });
    } else if (id == 2) {
      this.modalRef = this.modalService.open(this.recurring, {
        centered: true,
      });
    } else if (id == 3) {
      this.modalRef = this.modalService.open(this.increasingRecurring, {
        centered: true,
      });
    }

    this.modalRef.result.then(
      (result: any) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason: any) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        console.log(this.closeResult);
      }
    );
  }
}
