import { Component, OnInit, Input } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as converter from 'number-to-words';
declare var $: any;

@Component({
  selector: 'app-add-ons',
  templateUrl: './add-ons.component.html',
  styleUrls: ['./add-ons.component.css'],
})
export class AddOnsComponent implements OnInit {
  lifeSecure: any;
  addOnsForm: FormGroup;
  accidentCover = 0;
  plusDisabled = false;
  personalShield = false;
  closeResult = '';
  outputWords = '';
  coverageAmount = '10,00,000';

  buttons = {
    pair1: 'BACK',
    pair2: 'PROCEED',
  };
  private modalRef: any;

  constructor(
    public router: Router,
    private fb: FormBuilder,
    public modalService: NgbModal
  ) {
    this.addOnsForm = this.fb.group({
      coverage: ['10,00,000'],
    });
  }

  ngOnInit(): void {
    $('html, body').animate(
      {
        scrollTop: 0,
      },
      500
    );
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});
  }

  toggleBenefits(val: string) {
    if (val == 'lifeSecure') {
      this.lifeSecure = !this.lifeSecure;
    }
  }

  proceed() {
    this.router.navigate(['more-details']);
  }

  checkFreq(content: any) {
    this.modalRef = this.modalService.open(content, {
      size: 'sm',
      centered: true,
    });
    this.modalRef.result.then(
      (result: any) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason: any) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        console.log(this.closeResult);
      }
    );
  }

  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }

  close() {
    this.modalRef.close();
  }

  isNumberKey(event: any) {
    var charCode = event.which ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  numberWithCommas() {
    if (this.addOnsForm.controls['coverage'].value != '') {
      var result = this.addOnsForm.controls['coverage'].value.replace(/,/g, '');
      result = result.toString();
      var lastThree = result.substring(result.length - 3);
      var otherNumbers = result.substring(0, result.length - 3);
      if (otherNumbers != '') lastThree = ',' + lastThree;
      var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
      this.addOnsForm.controls['coverage'].setValue(res);

      this.outputWords = converter.toWords(res.replace(/,/g, ''));
      console.log(this.outputWords);
    } else {
      this.outputWords = '';
    }
  }

  updateCoverage() {
    this.coverageAmount = this.addOnsForm.controls['coverage'].value;
    this.modalRef.close();
  }

  decrease() {
    var result = this.addOnsForm.controls['coverage'].value.replace(/,/g, '');
    if (parseInt(result) > 10000) {
      result = parseInt(result) - 10000;
      this.commaConversion(result);
    }
  }
  increase() {
    var result = this.addOnsForm.controls['coverage'].value.replace(/,/g, '');
    result = parseInt(result) + 10000;
    this.commaConversion(result);
  }
  commaConversion(result: any) {
    result = result.toString();
    var lastThree = result.substring(result.length - 3);
    var otherNumbers = result.substring(0, result.length - 3);
    if (otherNumbers != '') lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
    this.addOnsForm.controls['coverage'].setValue(res);
  }
}
