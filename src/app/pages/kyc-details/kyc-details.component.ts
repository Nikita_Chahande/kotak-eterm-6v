import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-kyc-details',
  templateUrl: './kyc-details.component.html',
  styleUrls: ['./kyc-details.component.css']
})
export class KYCDetailsComponent implements OnInit {
  addressForm: FormGroup;

  enterAddress:boolean = true;
  permanentAddress:boolean = true;
  isPermanentAddres:boolean =false;
  isCurrentAddres: boolean = true;
  currentAddress:boolean = true;

  kyc:any;
  key:any;

  constructor(public router: Router, private fb: FormBuilder) {
    console.log(localStorage.getItem(this.key));
    
    this.addressForm = this.fb.group({
      address1: ['', Validators.required]
    });
   }

  ngOnInit(): void {
    
  }

  openConditions(event:any, getValue:any) {
    getValue = event.target.checked;
    this.enterAddress = !this.enterAddress;  
  }


  submitForm() {
    if(localStorage.getItem(this.key)=='details'){
      localStorage.setItem(this.kyc, 'viwed');
    }    
    this.router.navigate(['verify-application']);   
  }

}


