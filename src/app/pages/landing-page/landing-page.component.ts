import { Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal,ModalDismissReasons,
  NgbModalRef, } from '@ng-bootstrap/ng-bootstrap';


import { Observable } from 'rxjs';
import Inputmask from 'inputmask';
import * as $ from 'jquery' 

declare var $: any;

const emailList = [
  'rajesh.wadhwa@gmail.com',
  'rajesh.wadhwa@yahoo.com',
  'rajesh.wadhwa@outlook.com',
];

@Component({
  selector: 'app-landing-page',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  @ViewChild('myInput')
  myInputElementRef!: ElementRef;

  @ViewChild('content')
  content!: TemplateRef<any>;

  isDomain = false;
  domains = ['gmail.com', 'yahoo.com', 'outlook.com', 'icloud.com'];

  formSubmitted = false;
  landingPageForm: FormGroup;
  gender = 'male';
  tobacco = 'no';
  customerAge = 0;
  showMinor = false;
  minDate = { year: 1920, month: 1, day: 1 };
  acceptPolicy = true;
  showTerm = false;
  showproduct= false;
  checkForm = false;
  closeResult = '';

  private modalRef: any;
  customOptions= {
    loop: true,
    margin: 0,
    nav: false,
    lazyLoad: true,
    autoplay: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    autoplaySpeed: 800,
    autoplayTimeout: 3000,
    autoplayHoverPause: false,
    dots: true,
    items: 1,
    navSpeed: 700,
  };

  /* For Typehead */
  public emaildModel: any;
  // search = (text$: Observable<string>) =>
  //   text$.pipe(
  //     debounceTime(200),
  //     distinctUntilChanged(),
  //     map((term) =>
  //       term.length < 2
  //         ? []
  //         : emailList
  //             .filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
  //             .slice(0, 10)
  //     )
  //   );

  constructor(
    public router: Router,
    private fb: FormBuilder,
    public modalService: NgbModal
  ) {
    this.landingPageForm = this.fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.pattern("^[a-zA-Z.'-]{2,50}(?: [a-zA-Z.'-]{2,50})$"),
        ],
      ],
      birthdate: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.minLength(10)]],
      emailId: ['', [Validators.required, Validators.email]],
    });
  }

  ngOnInit(): void {
    // $('html, body').animate(
    //   {
    //     scrollTop: 0,
    //   },
    //   500
    // );
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});


  }

  ngAfterViewInit(): void {
    Inputmask('datetime', {
      inputFormat: 'dd/mm/yyyy',
      placeholder: 'dd/mm/yyyy',
      alias: 'datetime',
      min: '01/01/1945',
      clearMaskOnLostFocus: false,
    }).mask(this.myInputElementRef.nativeElement);
  }

  setGender(val: string) {
    this.gender = val;
  }

  isNumberKey(event: any) {
    var charCode = event.which ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  navigate() {
    //debugger;
    this.formSubmitted = true;

    if (this.landingPageForm.valid && this.acceptPolicy) {
      this.modalRef = this.modalService.open(this.content, {
        centered: true,
      });
      this.modalRef.result.then(
        (result: any) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason: any) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          console.log(this.closeResult);
        }
      );
    } else {
      setTimeout(() => {
        this.moveToError();
      }, 500);
    }
  }

  gotoResume() {
    this.router.navigate(['resume-page']);
  }

  setTobacco(val: any) {
    this.tobacco = val;
  }

  validate(evt: any) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  moveToError() {
    var elt = $('.errorInput');
    if (elt.length) {
      $('html, body').animate(
        {
          scrollTop: elt.first().offset().top - 90,
        },
        500
      );
    }
  }

  termClicked(status: any) {
    if (status) {
      this.showTerm = false;
    } else {
      this.showTerm = true;
    }
  }

  productClicked(status: any) {
    if (status) {
      this.showproduct = false;
    } else {
      this.showproduct = true;
    }
  }

  checkEmailDomain() {
    if (this.landingPageForm.controls['emailId'].value.indexOf('@') != '-1') {
      this.isDomain = true;
    } else {
      this.isDomain = false;
    }
  }

  offEmailDomain(event: any){
    setTimeout(() => 
    {
      this.isDomain = false;
    },
    500);
  }

  domainClick(val: any) {
    let tempEmail = this.landingPageForm.controls['emailId'].value;
    let splitted = tempEmail.split('@');

    this.landingPageForm.controls['emailId'].setValue(splitted[0] + '@' + val);
    this.isDomain = false;
  }

  cancel() {
    this.modalRef.close();
  }

  resume() {
    this.modalRef.close();
    this.router.navigate(['eligibility-criteria']);
  }

  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }


}
