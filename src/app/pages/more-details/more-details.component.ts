import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

declare var  $: any;

@Component({
  selector: 'app-more-details',
  templateUrl: './more-details.component.html',
  styleUrls: ['./more-details.component.css'],
})
export class MoreDetailsComponent implements OnInit {
  moreDetailsForm: FormGroup;
  formSubmitted = false;
  customerAge = 0;
  minDate = { year: 1920, month: 1, day: 1 };
  acceptPolicy = true;
  buttons = {
    pair1: 'BACK',
    pair2: 'MAKE PAYMENT',
  };
  $: any;

  userdetails = {
    name: 'Shyam Verma',
    birthdate: '13071988',
    gender: 'Male',
    email: 'shyam@yahoo.com',
    mobile: '9090908282',
  };

  constructor(public router: Router, private fb: FormBuilder) {
    this.moreDetailsForm = this.fb.group({
      name: [this.userdetails.name, Validators.required],
      pan: ['', Validators.required],
      email: [this.userdetails.email, Validators.required],
      mobile: [this.userdetails.mobile, Validators.required],
      altMobile: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    $('html, body').animate(
      {
        scrollTop: 0,
      },
      500
    );
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});
  }

  statusNavigate() {
    this.formSubmitted = true;
    if (this.moreDetailsForm.valid && this.acceptPolicy) {
      //this.router.navigate(['add-ons']);
      console.log('make payment');
    } else {
      setTimeout(() => {
        this.moveToError();
      }, 500);
    }
  }

  validate(evt: any) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  moveToError() {
    // var elt = $(".errorInput");
    // if (elt.length) {
    //   $('html, body').animate({
    //     scrollTop: (elt.first().offset().top) - 90
    //   }, 500);
    // }
  }
}
