import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
export interface Tab {
  title: string;
  active: boolean;
  iconClass: string;
  content: string;
};

@Component({
  selector: 'app-tabset',
  templateUrl: './tabset.component.html',
  styleUrls: ['./tabset.component.css']
})
export class TabsetComponent implements OnInit {


  constructor() {

  }

  ngOnInit(): void {
  }

}
