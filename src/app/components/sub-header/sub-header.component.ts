import { Component, ElementRef, Input, OnInit, TemplateRef, ViewChild, AfterViewInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditBasicDetailsComponent } from '../edit-basic-details/edit-basic-details.component';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Inputmask from 'inputmask';


@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit {


  @ViewChild('editDetails')
  editDetails!: TemplateRef<any>;

  editDetailsForm: FormGroup;
  gender = 'male';
  tobacco = 'no';
  minDate = { year: 1920, month: 1, day: 1 };
  modalRef: any;
  closeResult='';
  
  constructor(private fb: FormBuilder, public modalService: NgbModal) {
    this.editDetailsForm = this.fb.group({
      name: ['', Validators.required],
      birthdate: ['', Validators.required],
      email: ['', Validators.required],
    });
  }
  userdetails= {
    name: 'Shyam Verma',
    birthdate: '13071998',
    gender: 'Male',
    email: 'shyam@yahoo.com',
    mobile: '9090908282'
  }

  ngOnInit(): void {
    console.log(this.userdetails);
    this.editDetailsForm.patchValue(this.userdetails);
  }

  // ngAfterViewInit(): void {
  //   Inputmask('datetime', {
  //     inputFormat: 'dd/mm/yyyy',
  //     placeholder: 'dd/mm/yyyy',
  //     alias: 'datetime',
  //     min: '01/01/1945',
  //     clearMaskOnLostFocus: false,
  //   }).mask(this.myInputElementRef.nativeElement);
  // }

  setGender(val: string) {
    this.gender = val;
  }

  settobacco(val: string) {
    this.tobacco = val;
  }

  close() {
    this.modalRef.close();
  }
  
  editFormFeild() {
    this.modalRef = this.modalService.open(this.editDetails, {
      centered: true,
    }); 

    this.modalRef.result.then(
      (result: any) => {
        this.closeResult = `Closed with: ${result}`;
      }
    );
  }
  content(content: any, arg1: { centered: boolean; }): any {
    throw new Error('Method not implemented.');
  }

  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }

  cancel() {
    this.modalRef.close();
  }
}
