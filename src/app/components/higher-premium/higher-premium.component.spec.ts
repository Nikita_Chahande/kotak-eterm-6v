import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HigherPremiumComponent } from './higher-premium.component';

describe('HigherPremiumComponent', () => {
  let component: HigherPremiumComponent;
  let fixture: ComponentFixture<HigherPremiumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HigherPremiumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HigherPremiumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
