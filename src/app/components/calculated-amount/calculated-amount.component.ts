import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calculated-amount',
  templateUrl: './calculated-amount.component.html',
  styleUrls: ['./calculated-amount.component.css'],
})
export class CalculatedAmountComponent implements OnInit {
  @Output('proceedWithStatus') proceedWithStatus: EventEmitter<any> =
    new EventEmitter();
  isDiscount: boolean = false;
  @Input() buttons: any = {};
  constructor(public router: Router) {}

  ngOnInit() {}

  proceedVal() {
    this.proceedWithStatus.emit();
  }

  getDiscount() {
    if (this.isDiscount) {
      this.isDiscount = false;
    } else {
      this.isDiscount = true;
    }
  }
  root() {
    console.log(this.router.url);
    if (this.router.url === '/more-details') {
      //  return "make_payment";
    }
  }
  back() {
    console.log(this.router.url);
    if (this.router.url == '/customize-plan') {
      this.router.navigate(['eligibility-criteria']);
    } else if (this.router.url == '/add-ons') {
      this.router.navigate(['customize-plan']);
    } else if (this.router.url == '/more-details') {
      this.router.navigate(['add-ons']);
    }
  }
}
